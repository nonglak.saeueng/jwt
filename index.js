const {
  getUserDetail,
  getJwt,
} = require("./Backend/Apis");
const { getGuidArray } = require("./Backend/readWriteFile");

// TODO: specify the path of the CSV file
const path = "./source/user.csv";

async function generateJWT(number) {
  const guids = await getGuidArray(path);
  for (const i = 0; i < number; i++) {
    const { email } = await getUserDetail(guids[i]);
    const jwt = await getJwt(guids[i], email, "1234");
    console.log(jwt);
  }
}

generateJWT(2);


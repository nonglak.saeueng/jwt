/* eslint-disable no-undef */
const axiosClient = require("./AxiosClient");
const client = {
  get: async (url, options) => {
    const response = await axiosClient.get(url, options);
    return response;
  },
  delete: async (url, options) => {
    const response = await axiosClient.delete(url, options);
    return response;
  },
  post: async (url, body, options) => {
    const response = await axiosClient.post(url, body, options);
    return response;
  },
  put: async (url, body, options) => {
    const response = await axiosClient.put(url, body, options);
    return response;
  },
};

module.exports = client;

const OAuthHelper = require("./OAuthHelper");
const client = require("./client");
const dotenv = require("dotenv");
dotenv.config({ path: ".env.beta" });

const AUTH_ENDPOINT = "/v3/xauth/access_token.json";
const JWT_TOKEN_ENDPOINT = "/cmw/v1/client/jwt";
const userAgent = "Mozilla/5.0";

async function getUserDetail(userGuid) {
  const request = {
    url: `${process.env.ums_host}/v5/user/${userGuid}/info.json`,
    method: "GET",
  };
  const headers = {
    ...OAuthHelper.getAuthHeaderForRequest(request),
  };

  try {
    // Send the HTTP PUT request
    const response = await client.get(request.url, {
      headers: headers,
    });
    // this.authData = response.data;
    return response.data;
  } catch (err) {
    throw new Error(`UMS call : getAuthData() : ${request.url} -> ${err}`);
  }
}

async function getAuthData(username, password = 1234) {
  // Constructing PUT request to hit /v3/xauth/access_token.json API
  const request = {
    url: `${process.env.ums_host}${AUTH_ENDPOINT}`,
    method: "PUT",
    body: {
      email: username,
      password: password,
      device_guid: "mantis",
    },
  };
  // Construct the request headers
  const headers = {
    ...OAuthHelper.getAuthHeaderForRequest(request),
    "user-agent": userAgent,
  };

  try {
    // Send the HTTP PUT request
    const response = await client.put(request.url, request.body, {
      headers: headers,
    });
    // this.authData = response.data;
    return response.data;
  } catch (err) {
    throw new Error(
      `UMS call : getAuthData() : ${request.url} -> ${err} --> ${username}`,
    );
  }
}

async function getJwt(guid, username, password) {
  const request = {
    url: `${process.env.cmw_host}${JWT_TOKEN_ENDPOINT}`,
    method: "POST",
    body: {
      device_guid: `${Date.now()}-mantis`,
      platform: process.env.platform,
      product: process.env.product,
      profile_guid: guid,
    },
  };
  try {
    const tokenResp = await getAuthData(username, password);
    const token = {
      key: tokenResp.oauth_token,
      secret: tokenResp.oauth_token_secret,
    };
    const response = await client.post(request.url, request.body, {
      headers: OAuthHelper.getAuthHeaderForRequest(request, token),
    });
    return response.data.jwt;
  } catch (error) {
    console.error("Error while getting JWT token:", error);
    throw error;
  }
}

module.exports = {
  getAuthData,
  getUserDetail,
  getJwt,
};

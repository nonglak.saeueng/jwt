/* eslint-disable no-undef */
const axios = require("axios");
const client = axios.create();

var startTime, endTime;

client.defaults.timeout = 180000;
client.interceptors.request.use(
  (request) => {
    let message = {
      method: request.method,
      url: request.url,
      body: request.data,
      slingInteractionId: request.headers["sling-interaction-id"],
    };
    console.log("Starting Request", JSON.stringify(message, null, 2));
    let requestDataToLog = JSON.parse(JSON.stringify(message));
    if (requestDataToLog.body && requestDataToLog.body.password) {
      delete requestDataToLog.body.password;
    }
    startTime = new Date().getTime();
    return request;
  },
  (error) => {
    throw error.message;
  },
);

client.interceptors.response.use(
  (response) => {
    endTime = new Date().getTime();
    if (endTime - startTime > 60000) {
      throw `Response took longer than 1 minute time: Actual time taken: ${endTime - startTime}`;
    }
    console.log(
      `${response.status} : ${response.config.method} ${response.config.url} - ${endTime - startTime} ms`,
    );
    return response;
  },
  (error) => {
    let message = { msg: error.message };
    if (error.response) {
      message.response = {
        status: error.response.status,
        headers: error.response.headers,
        body: error.response.data,
      };
      console.log(error.message);
    }
    let title = "Error in request";
    if (error.config) {
      title = `Axios-Error: ${error.config.method} ${error.config.url}`;
      console.log(error.config);
    }
    console.log(title);
    throw error;
  },
);
module.exports = client;

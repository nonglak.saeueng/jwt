const fs = require("fs-extra");
const { parse } = require("csv-parse");

async function readFile(path) {
  const data = await fs.promises.readFile(
    path,
    "utf8",
    async function (err, data) {
      if (err) {
        console.error("Error while reading:", err);
        return;
      }
    },
  );
  return data;
}

function getData(data, index) {
  const lines = data.split("\n");
  const output = [];
  lines.forEach((line) => {
    const fields = line.split(",");
    const item = fields[index].replace("\r", '')
    output.push(item);
  });
  return output;
}

async function getGuidArray(path) {
  const data = await readFile(path);
  return getData(data, 1);
}
module.exports = {
  readFile,
  getGuidArray,
};
